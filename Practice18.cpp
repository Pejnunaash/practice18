﻿#include <iostream>

template<typename T>
class stack {
private:    
    int n=6;
    T* stck = new T[n];
    int top=0;
public:
    stack<T>() {
        std::cout << "zadayte razmer massiva" << '\n';
        std::cin >> n;
   }
    T pop() {
        return stck[top--];
    }
    void push(T var) {
        stck[top++] = var;
    }
    void ShowStack() {
        for (int i = 0; i < top; i++) {
            std::cout << stck[i]<<'\n';
       }
    }
    void FckMemoryLeak() {
        delete stck;
    }
};

int main()
{
    stack<int> s1;
    s1.push(1);
    s1.push(22);
    s1.push(333);
    s1.push(4444);
    s1.push(55555);
    s1.push(666666);
    s1.ShowStack();

    s1.pop();
    s1.ShowStack();

    s1.push(1337);
    s1.ShowStack();

    s1.FckMemoryLeak();
    
}